﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nepritel : MonoBehaviour
{
    #region Public Variables
    public Transform rayCast;
    public LayerMask rayCastMask;
    public float rayCastLength;
    public float delkaUtoku;
    public float timer;
    #endregion
    #region Private Variables
    private RaycastHit2D trefa;
    private GameObject cil;
    private float vzdalenost;
    private bool utocnyMod;
    private bool naDosah;
    private bool cooldown=true;
    private float intTimer;
    private float rych;
    #endregion
    zivoty zivoty = new zivoty();
    public Animator animator;
    public int maxHP = 100;
    public float dalka = 0.5f;
    int momentHP;
    public float rychlost;
    public bool pohybVpravo;   
   
    private void Awake()
    {
        transform.Rotate(0f,0f,0f);
        intTimer = timer;
        animator = GetComponent<Animator>();
    }
    void OnTriggerEnter2D (Collider2D other)
    {
       if(other.gameObject.tag == "Player")
        {
            cil = other.gameObject;
            naDosah = true;
            if (cooldown==false)
            {

                other.GetComponent<zivoty>().DostanPoskozeni(1);
                animator.SetBool("attack", true);
                
            }

        }
        if (other.gameObject.tag == "turn")
        {
            if (pohybVpravo)
            {
                pohybVpravo = false;
                Debug.Log("narazil jsem");
                transform.Rotate(0f, 180f, 0f);


            }
            else
            {
                pohybVpravo = true;
                Debug.Log("narazil jsem 2");
                transform.Rotate(0f, 0f, 0f);

            }
        }
    }
    void Start()
    {
        momentHP = maxHP;

    }
    private void Update()
    {
        if (naDosah)
        {
            trefa = Physics2D.Raycast(rayCast.position, Vector2.left, rayCastLength, rayCastMask);
            RaycastDebugger();
        }
        if (pohybVpravo)
        {
            rych = 2 * Time.deltaTime * rychlost;
           transform.Translate(2 * Time.deltaTime * rychlost, 0, 0);

        }
        else
        {
             rych = -2 * Time.deltaTime * rychlost;
            transform.Translate(-2 * Time.deltaTime * rychlost, 0, 0);
            

        }

        if (trefa.collider != null)
        {
            NepritelLogika();
        }
        else if (trefa.collider == null)
        {
            naDosah = false;
        }
        if(naDosah == false)
        {
            ZastavUtok();
            
        }

    }
    void NepritelLogika()
    {
        vzdalenost = Vector2.Distance(transform.position, cil.transform.position);
        if(delkaUtoku >= vzdalenost && cooldown == false)
        {
            Utok();
            cooldown = true;

        }
        if (cooldown)
        {

            Debug.Log("jsem unavenej");

            Cooling();

        }
    }
    private void Cooling()
    {

        timer -= Time.deltaTime;
        if(timer<=0 && cooldown && utocnyMod)
        {
            cooldown = false;
            timer = intTimer;
        }

    }
    void Utok()
    {

        timer = intTimer;
        utocnyMod = true;
        animator.SetBool("attack", true);
        
    }
    void ZastavUtok()
    {
        cooldown = false;
        utocnyMod = false;
        animator.SetBool("attack", false);
    }
    public void DostanPoskozeni(int poskozeni)
    {
        momentHP -= poskozeni;
        animator.SetTrigger("Ubliz");
        if(momentHP <= 0)
        {
            animator.SetBool("Mrtvy", true);
            Invoke("Smrt",2f);   
        }
    }
    public void Smrt()
    {
        Debug.Log("Enemy Died");
        GetComponent<Collider2D>().enabled = false;
        DestroyObject(gameObject);
    }

    void RaycastDebugger()
    {
        if(vzdalenost > delkaUtoku)
        {
            Debug.DrawRay(rayCast.position, Vector2.left * rayCastLength, Color.red);
        }
        else if(delkaUtoku> vzdalenost)
        {
            Debug.DrawRay(rayCast.position, Vector2.left * rayCastLength, Color.green);
        }
    }
    public void TriggerCooldown()
    {
        cooldown = true;
    }
}
