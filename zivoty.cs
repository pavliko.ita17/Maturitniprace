﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class zivoty : MonoBehaviour
{
    public int zivot;
    public int pocetZivot;
    public Image[] obrazky;
    public Sprite plneSrdce;
    public Sprite prazdneSrdce;
    public Collider2D box;
    public Animator anim;

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < obrazky.Length; i++)
        {
            if (i < zivot)
            {
                obrazky[i].sprite = plneSrdce;

            }
            else
            {
                obrazky[i].sprite = prazdneSrdce;
            }
            if (i < pocetZivot)
            {
                obrazky[i].enabled = true   ;
            }
            else
            {
                obrazky[i].enabled = false;
            }
           
        }

    }
    public void DostanPoskozeni(int poskozeni)
    {
        zivot -= poskozeni;
        if (zivot < pocetZivot)
        {
            
            anim.SetTrigger("Ublizen");
        }
       
        if (zivot <= 0)
        {
            anim.SetBool("Mrtev", true);
            SceneManager.LoadScene("UmrtiLevel1");
            
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Damage")
        {
            DostanPoskozeni(1);
        }
        if (collision.gameObject.tag == "Death") 
        {
            DostanPoskozeni(99999);
        }
        if (collision.gameObject.tag == "EndOFLevel")
        {
            Debug.Log("Najetý");
            SceneManager.LoadScene("Vyhra");
        }

    }



}
