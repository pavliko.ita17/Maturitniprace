﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HracSouboj : MonoBehaviour
{
    public Animator animator;
    public Transform utocnyBod;
    public float dalka = 0.5f;
    public LayerMask nepratelskeVrstvy;
    public int poskozeni = 40;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Utok();
        }
    }
    void Utok()
    {   
        animator.SetTrigger("Utocim");
        Collider2D[] hit = Physics2D.OverlapCircleAll(utocnyBod.position,dalka,nepratelskeVrstvy);
        foreach(Collider2D nepritel in hit)
        {
            nepritel.GetComponent<Nepritel>().DostanPoskozeni(poskozeni);
        }
    }
    private void OnDrawGizmosSelected()
    {
        if (utocnyBod == null) return;
        Gizmos.DrawWireSphere(utocnyBod.position, dalka);
    }

}
